import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterProblemsComponent } from './register-problems.component';

describe('RegisterProblemsComponent', () => {
  let component: RegisterProblemsComponent;
  let fixture: ComponentFixture<RegisterProblemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterProblemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterProblemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
