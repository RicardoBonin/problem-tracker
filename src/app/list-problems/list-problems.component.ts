import { ProblemServiceService } from './../problem-service.service';
import { ProblemServiceDataService } from './../problem-service-data.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-list-problems',
  templateUrl: './list-problems.component.html',
  styleUrls: ['./list-problems.component.css']
})
export class ListProblemsComponent implements OnInit {

  problems: Observable<any>;

  constructor(private problemService: ProblemServiceService,
              private problemDataService: ProblemServiceDataService) { }

  ngOnInit() {
    this.problems = this.problemService.getAll();
  }

}
