import { ProblemList } from './problem-list';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProblemServiceService {

  constructor(private db: AngularFireDatabase) { }
  insert(ProblemList: ProblemList){
    this.db.list("problemLis").push(ProblemList)
      .then((result: any)  => {
        console.log(result.key);
      });

  }
  getAll() {
    return this.db.list('problemLis')
    .snapshotChanges()
    .pipe(
      map(changes => {
        return changes.map(data => ({ key: data.payload.key, ...(data.payload.val() as object)  }));
      })
    )
  }
}
