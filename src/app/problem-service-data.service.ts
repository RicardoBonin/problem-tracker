import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProblemServiceDataService {

  constructor() { }
  private problemSource = new BehaviorSubject({ contato: null, key: '' });
  problemAtual = this.problemSource.asObservable();


}
