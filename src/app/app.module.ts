
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterProblemsComponent } from './register-problems/register-problems.component';
import { ProblemServiceService } from './problem-service.service';

import { environment } from '../environments/environment';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule} from '@angular/fire/database';
import { ListProblemsComponent } from './list-problems/list-problems.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterProblemsComponent,
    ListProblemsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule
  ],
  providers: [ProblemServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
