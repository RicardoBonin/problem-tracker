import { TestBed } from '@angular/core/testing';

import { ProblemServiceDataService } from './problem-service-data.service';

describe('ProblemServiceDataService', () => {
  let service: ProblemServiceDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProblemServiceDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
