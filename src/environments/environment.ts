// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCaVht9bHzjCDRqxXZY-GygrtdNSb05vHU",
    authDomain: "problem-tracker.firebaseapp.com",
    databaseURL: "https://problem-tracker.firebaseio.com",
    projectId: "problem-tracker",
    storageBucket: "problem-tracker.appspot.com",
    messagingSenderId: "636537506582",
    appId: "1:636537506582:web:5cf9788f1fbe7bd0c25e37"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
